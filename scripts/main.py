from azure_storage_operations import StorageHealth
from os import getenv


class Parameters():
    def __init__(self):
        self.azure_connection_string         = getenv('AZURE_STORAGE_CONNECTION_STRING')
        self.sleep_time                      = getenv('SLEEP_TIME')
        self.max_retries                     = getenv('MAX_RETRIES')
        self.container_name                  = getenv('CONTAINER_NAME')
        self.local_path_files                = getenv('LOCAL_PATH_FILES')
        self.enable_failover                 = getenv('ENABLE_FAILOVER')
        self.resource_group                  = getenv('RESOURCE_GROUP')
        self.subscription_id                 = getenv('SUBSCRIPTION_ID')
        self.storage_type_switch             = getenv('STORAGE_TYPE_SWITCH')

if __name__ == "__main__":
    parameters = Parameters()
    health_check = StorageHealth(
        parameters.azure_connection_string,
        int(parameters.max_retries),
        int(parameters.sleep_time),
        parameters.container_name,
        parameters.local_path_files,
        parameters.resource_group,
        parameters.subscription_id,
        parameters.enable_failover,
        parameters.storage_type_switch
    )

    health_check.get_credentials()
    health_check.perform()
 