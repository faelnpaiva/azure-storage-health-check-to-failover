import os,shutil,time
from azure.storage.blob import BlobServiceClient, ContainerClient
from datetime import datetime
import subprocess
import json
from azure.identity import ClientSecretCredential
from azure.mgmt.compute import ComputeManagementClient
from azure.mgmt.resource import ResourceManagementClient
from azure.mgmt.storage import StorageManagementClient
from azure.mgmt.storage.models import (
    StorageAccountCreateParameters,
    StorageAccountUpdateParameters,
    Sku,
    SkuName,
    Kind
)

GEO_REPLICATION_STORAGE_TYPES = ['Standard_GRS', 'Standard_ZRS', 'Standard_GZRS']

class StorageHealth():
    def __init__(self, azure_connection_string, max_retries, sleep_time, container_name, path_files, resource_group, subscription_id, enable_failover, storage_type_switch):
        self.access                 = azure_connection_string
        self.credentials, self.subscription_id = self.get_credentials()
        self.storage_client_mngmt = StorageManagementClient(self.credentials, self.subscription_id)
        self.resource_client = ResourceManagementClient(self.credentials, self.subscription_id)
        self.max_retries            = max_retries
        self.sleep_time             = sleep_time
        self.container_name         = container_name
        self.blob_service_client    = BlobServiceClient.from_connection_string(self.access)
        self.local_path             = path_files
        self.create_folder(self.local_path)
        self.local_file_name        = str(self.container_name) + datetime.today().strftime('%Y-%m-%d-%H:%M:%S') + ".txt"
        self.upload_file_path       = os.path.join(self.local_path,self.local_file_name)
        self.create_file()
        self.download_file_path     = os.path.join(self.local_path, str.replace(self.local_file_name ,'.txt', '-DOWNLOAD.txt'))
        self.container_client       = None
        self.blob_client            = None
        self.resource_group         = resource_group
        self.subscription_id        = subscription_id
        self.enable_failover        = enable_failover
        self.account_name           = azure_connection_string.split(';')[1].split('=')[1]
        self.storage_gr_stats       = None
        self.storage_type_switch    = storage_type_switch

    def get_credentials(self):
        subscription_id = os.environ.get('SUBSCRIPTION_ID')  
        credentials = ClientSecretCredential(
            client_id=os.environ['USER'],
            client_secret=os.environ['PASSWORD'],
            tenant_id=os.environ['TENTANT_ID']
        )
        return credentials, subscription_id

    def perform(self):
        print("Starting health check")
        if self.failover_is_in_progress():
            print("Failover is in progress. Aborting health check.")
            exit(0)
        elif self.can_failover():
            call_failover = True
            self.storage_connect(attempt_count=0, call_failover=call_failover)
            self.upload_file(attempt_count=0, call_failover=call_failover)
            self.download_file(attempt_count=0, call_failover=call_failover)
        else:
            self.storage_to_grs() 
            call_failover = False
            self.storage_connect(attempt_count=0, call_failover=call_failover)
            self.upload_file(attempt_count=0, call_failover=call_failover)
            self.download_file(attempt_count=0, call_failover=call_failover)
            
        self.cleanup()

    def storage_connect(self, attempt_count, call_failover):
        try:
           self.create_container()
           print("Connected to Storage Account!")
        except:
            if int(attempt_count) < self.max_retries:
                attempt_count += 1
                print("Connection failed! Retrying: " + str(attempt_count))
                time.sleep(self.sleep_time)
                return self.storage_connect(attempt_count, call_failover)
            else:
                print("Maximum connection attempts reached!")
                if call_failover:
                    print("Triggering failover...")
                    self.trigger_failover()
                else:
                    print("Failover not will be triggered because storage doesn't support yet!")
                exit(1)


    def container_exists(self):
        checking = ContainerClient.from_connection_string(self.access,self.container_name)
        try:
           checking.get_container_properties()
           container = True
        except:
           container = False
        return container

    def create_folder(self,path):
        if not os.path.exists(path):
            os.mkdir(path)

    def create_file(self):
        file = open(self.upload_file_path, 'w')
        file.close()

    def create_container(self):
        try:
          if self.container_exists():
            container_client = self.blob_service_client.get_container_client(self.container_name)
          else:
            container_client = self.blob_service_client.from_connection_string(self.access).create_container(self.container_name)
          self.container_client = container_client
        except Exception as ex:
           template = "An exception of type {0} occurred. Arguments:\n{1!r}"
           message = template.format(type(ex).__name__, ex.args)
           print(message)
           raise Exception(message)

    def upload_file(self, attempt_count, call_failover):
        self.blob_client = self.blob_service_client.get_blob_client(container=self.container_name, blob=self.local_file_name)
        print("Uploading to Azure Storage as blob: " + self.local_file_name)
        with open(self.upload_file_path, "rb") as data:
            try:
                self.blob_client.upload_blob(data)
                print("File Uploaded!")
            except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                print(message)
                if attempt_count < self.max_retries:
                   attempt_count += 1
                   print("Uploading file failed! Retrying: " + str(attempt_count) )
                   time.sleep(self.sleep_time)
                   return self.upload_file(attempt_count, call_failover)
                else:
                    print("Maximum upload attempts reached!")
                    if call_failover:
                        print("Triggering failover...")
                        self.trigger_failover()
                    else:
                        print("Failover not will be triggered because storage doesn't support!")
                    exit(1)

    def download_file(self, attempt_count, call_failover):
        print("Downloading blob: " + self.download_file_path)
        with open(self.download_file_path, "wb") as download:
            try:
                download.write(self.blob_client.download_blob().readall())
                print("File Downloaded!")
            except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                print(message)
                if attempt_count < self.max_retries:
                   attempt_count += 1
                   print("Downloading file failed! Retrying: " + str(attempt_count) )
                   time.sleep(self.sleep_time)
                   return self.download_file(attempt_count, call_failover)
                else:
                    print("Maximum download attempts reached!")
                    if call_failover:
                        print("Triggering failover...")
                        self.trigger_failover()
                    else:
                        print("Failover not will be triggered because storage doesn't support!")
                    exit(1)

    def cleanup(self):
        print("Deleting blob container file: "+ os.path.join(self.container_name,self.local_file_name))
        if self.container_exists():
            self.container_client.delete_blob(self.local_file_name)
            print("Deleting the local source and downloaded files...")
            shutil.rmtree(self.local_path)
            print("Done")
        else:
            print("Couldn't delete the container ! Container doesn't exist..")
    
    def get_storage_type(self):
        storage_type=self.storage_client_mngmt.storage_accounts.get_properties(self.resource_group,self.account_name)
        try:
            if storage_type.sku.name:
                return storage_type.sku.name
            else:
                raise Exception("No data from Storage properties")
        except:
            exit(1)
    
    def storage_supports_failover(self):
        storage_type = self.get_storage_type()
        if storage_type not in GEO_REPLICATION_STORAGE_TYPES:
            return False
        return True

    def get_geo_replication_stats(self):
        if self.storage_supports_failover():
            try:
                data = self.storage_client_mngmt.storage_accounts.get_properties(self.resource_group,self.account_name,expand='geoReplicationStats')
                return data
            except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                print(message)
                return None
        else:
            return None

    def can_failover(self):
        if self.storage_supports_failover():
            try:
                if self.enable_failover == "True":
                    print("Failover flag is enabled!")
                    geo_replication_stats = self.get_geo_replication_stats()
                    if geo_replication_stats.geo_replication_stats.can_failover and geo_replication_stats.geo_replication_stats.last_sync_time:
                        return True
                return False
            except Exception as e:
                print(e)
        else:
            return False

    def failover_is_in_progress(self):
        try:
            if self.storage_supports_failover():
                geo_replication_stats = self.get_geo_replication_stats()
            else:
                print("Storage doesn't support failover yet!")
                return False
            if geo_replication_stats.failover_in_progress:
                return True
            return False
        except Exception as e:
            print(e)

    def trigger_failover(self):
        if self.can_failover():
            self.storage_client_mngmt.storage_accounts.begin_failover(self.resource_group,self.account_name)
            print("Failover executing now ...")
        else:
            print(f'Storage "{self.account_name}" does not support failover yet!')

    def storage_to_grs(self):
        try:
            if self.storage_type_switch == "True" and not self.storage_supports_failover():
               self.storage_client_mngmt.storage_accounts.update(self.resource_group , self.account_name, StorageAccountUpdateParameters(sku=Sku(name=SkuName.standard_grs)))
               print("Switching storage type to GRS!")
            else:
                print("Switch storage type is not allowed or Storage is already GRS")
        except Exception as e:
            print("Couldn't switch storage to GRS!" + e)
  
