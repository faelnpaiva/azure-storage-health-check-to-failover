#!/bin/bash +x 

#Python dependencies
/usr/local/bin/python -m pip install --upgrade pip -q
pip install -r /etc/config/requirements.txt -q
#Execute script
python -u /etc/config/main.py